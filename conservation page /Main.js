
function openLink(url) {
  var message = 'Please confirm whether you would like to leave the page.';

  if (confirm(message)) {
    window.open(url, '_blank');
  }
}
